# C++  Project Template
######JRODDEV 2018


###Getting Started
After pulling the repository you will need to `run setup.sh` to grab the 3rd party library files (googletest by default). 
The setup script will also build and run the test variant and build the debug variant.  
After these are sync'd you can start building the targets e.g. `make debug`.  

To change project configuration and add third party libraries see `app.config`. You shouldn't need to change the `Makefile` unless you want structural changes in the template. 

##

###Targets
- `all`: builds debug, release and test targets.   
- `debug`: Standard build used for debugging.  
- `release`: Optimized production build (but it's just the same as debug at the moment).  
- `test`: google test build. Remember to write tests.  
- `clean`: will remove `build` directory and run all `{subproject}-clean` targets.
##
###Directories
##### `src`
- Put all your .cpp and .h files in here

##### `libs`
- Third party libraries go in here

##### `build` (Generated files)
- `debug`, `release` and `test` directories for build variants
- `bin`: for the executable
- `obj`: for intermediate compiled obj files
- `dep`: automatic dependency generation files (`-MT -MD -MP -MF`)

##
###Third Party Libraries
By default the `googletest` library is included as a `git submodule` and setup to be built and linked with the project.  
You will need to add an entry in app.config (see the googletest section as an example).

####Folder Structure
- libs/googletest/  (top level library folder)
 - googletest (the actual submodule src) 
 - Makefile (write a Makefile that builds the subproject and copies headers and lib files) 
 - include (copy header files here)
 - lib (copy compiled libs here)


##

###VS Code Extensions
- C/C++ by Microsoft
- C/C++ Advanced Lint by Joseph Benden
- Clang Formate by xaver
- C/C++ Clang Command Adapter by Yasuaki MITANI



##

###Notes

##### BASED ON
- https://gist.github.com/maxtruxa/4b3929e118914ccef057f8a05c614b0f


##### RESOURCES
- https://stackoverflow.com/questions/2481269/how-to-make-a-simple-c-makefile
- https://spin.atomicobject.com/2016/08/26/makefile-c-projects/
- https://www.cyberciti.biz/faq/find-command-exclude-ignore-files/
- https://clang.llvm.org/cxx_status.html
