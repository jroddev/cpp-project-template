
#include "placeholderTest.h"
#include "gtest/gtest.h"

TEST(PlaceholderTest, testAdd) {
  EXPECT_EQ(PlaceholderTest::add(1, 1), 2);
  EXPECT_EQ(PlaceholderTest::add(5, 1), 6);
  EXPECT_EQ(PlaceholderTest::add(2, 9), 11);
}
