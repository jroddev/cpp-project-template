#include <iostream>

#include "main.h"
#include "subdir/subdir.h"
#include "test/placeholderTest.h"

using namespace std;

int main() {
  cout << "Hello: :" << testVariable << endl;
  auto subdir = Subdir{};
  cout << "Subdir: " << subdir.greeting << " Count: " << subdir.count << endl;
  cout << "test add: " << PlaceholderTest::add(testVariable, subdir.count)
       << endl;
}
