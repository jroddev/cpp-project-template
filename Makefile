# CPP MAKE
# jroddev 2018

# Configuration: Default

CXX := clang++	# compiler
LD := clang++	# linker
# C++ flags
CXXFLAGS := -std=c++17 
# C/C++ Flags
CPPFLAGS := -g -Wall -Wextra -pedantic -Isrc
# linker flags
LDFLAGS :=
DEPFLAGS = -MT $@ -MD -MP -MF
# flags required for dependency generation; passed to compilers

BUILD_DIR:=build
SUBPROJECTS= # Subproject get appended in app.config file

# Configuration: Application
include ./app.config

.PHONY: help
help:
	@echo available targets: all debug release test clean

all: debug release test

.PHONY: clean
clean: $(patsubst %, %-clean, $(SUBPROJECTS))
	rm -rf build

#################################################################################
###########################   DEBUG   ###########################################
#################################################################################

DEBUG_APPLICATION_NAME=debug-$(APPLICATION)-$(VERSION)
DEBUG_SRCS = $(shell find ./src -name *.cpp ! -name *.test.*)
DEBUG_OBJ_DIR = $(BUILD_DIR)/debug/obj
DEBUG_OBJS = $(patsubst %,$(DEBUG_OBJ_DIR)/%.o, $(basename $(DEBUG_SRCS)))
DEBUG_DEP_DIR= $(BUILD_DIR)/debug/dep
DEBUG_DEPFLAGS = $(DEPFLAGS) $(DEBUG_DEP_DIR)/$*.Td
DEBUG_DEPS := $(patsubst %,$(DEBUG_DEP_DIR)/%.Td, $(basename $(DEBUG_SRCS)))

.PHONY: debug
debug: $(SUBPROJECTS) $(BUILD_DIR)/debug/bin/$(DEBUG_APPLICATION_NAME)

$(BUILD_DIR)/debug/bin/$(DEBUG_APPLICATION_NAME): $(DEBUG_OBJS)
	mkdir -p $(dir $@)
	$(LD) -o $@ $^ $(LDFLAGS) $(LDLIBS)
	ln -Sf "$@" "$(BUILD_DIR)/latest_debug"


$(DEBUG_OBJ_DIR)/%.o: %.cpp
$(DEBUG_OBJ_DIR)/%.o: %.cpp $(DEBUG_DEP_DIR)/%.d
	mkdir -p $(dir $@)
	mkdir -p $(patsubst build/debug/obj/%, build/debug/dep/%, $(dir $@))
	$(CXX) $(DEBUG_DEPFLAGS) $(CXXFLAGS) $(CPPFLAGS) $(EXTERNAL_INCLUDE) -c -o $@ $<


# .PRECIOUS: if make is killed or interrupted during the execution of their recipes, the target is not deleted
.PRECIOUS = $(DEBUG_DEP_DIR)/%.d
$(DEBUG_DEP_DIR)/%.d: ;

-include $(DEBUG_DEPS)


#################################################################################
#########################   RELEASE   ###########################################
#################################################################################

RELEASE_APPLICATION_NAME=release-$(APPLICATION)-$(VERSION)
RELEASE_SRCS = $(shell find ./src -name *.cpp ! -name *.test.*)
RELEASE_OBJ_DIR = $(BUILD_DIR)/release/obj
RELEASE_OBJS = $(patsubst %,$(RELEASE_OBJ_DIR)/%.o, $(basename $(RELEASE_SRCS)))
RELEASE_DEP_DIR= $(BUILD_DIR)/release/dep
RELEASE_DEPFLAGS = $(DEPFLAGS) $(RELEASE_DEP_DIR)/$*.Td
RELEASE_DEPS := $(patsubst %,$(RELEASE_DEP_DIR)/%.Td, $(basename $(RELEASE_SRCS)))
RELEASE_CXXFLAGS =

.PHONY: release
release: $(SUBPROJECTS) $(BUILD_DIR)/release/bin/$(RELEASE_APPLICATION_NAME)

$(BUILD_DIR)/release/bin/$(RELEASE_APPLICATION_NAME): $(RELEASE_OBJS)
	mkdir -p $(dir $@)
	$(LD) -o $@ $^ $(LDFLAGS) $(LDLIBS)
	ln -Sf "$@" "$(BUILD_DIR)/latest_release"


$(RELEASE_OBJ_DIR)/%.o: %.cpp
$(RELEASE_OBJ_DIR)/%.o: %.cpp $(RELEASE_DEP_DIR)/%.d
	mkdir -p $(dir $@)
	mkdir -p $(patsubst build/release/obj/%, build/release/dep/%, $(dir $@))
	$(CXX) $(RELEASE_DEPFLAGS) $(CXXFLAGS) $(RELEASE_CXXFLAGS) $(CPPFLAGS) $(EXTERNAL_INCLUDE) -c -o $@ $<


# .PRECIOUS: if make is killed or interrupted during the execution of their recipes, the target is not deleted
.PRECIOUS = $(RELEASE_DEP_DIR)/%.d
$(RELEASE_DEP_DIR)/%.d: ;

-include $(RELEASE_DEPS)


#################################################################################
###########################   TEST   ############################################
#################################################################################

TEST_APPLICATION_NAME=test-$(APPLICATION)-$(VERSION)
TEST_SRCS = $(shell find ./src -name *.cpp ! -name main.cpp)
TEST_OBJ_DIR = $(BUILD_DIR)/test/obj
TEST_OBJS = $(patsubst %,$(TEST_OBJ_DIR)/%.o, $(basename $(TEST_SRCS)))
TEST_DEP_DIR= $(BUILD_DIR)/test/dep
TEST_DEPFLAGS = $(DEPFLAGS)  $(TEST_DEP_DIR)/$*.Td
TEST_DEPS := $(patsubst %,$(TEST_DEP_DIR)/%.Td, $(basename $(TEST_SRCS)))
TEST_CXXFLAGS = -DTEST_BUILD 

.PHONY: test
test: $(SUBPROJECTS) $(BUILD_DIR)/test/bin/$(TEST_APPLICATION_NAME)

$(BUILD_DIR)/test/bin/$(TEST_APPLICATION_NAME): $(TEST_OBJS)
	mkdir -p $(dir $@)
	$(LD) -o $@ $^ $(LDFLAGS) $(LDLIBS)
	ln -Sf "$@" "$(BUILD_DIR)/latest_test"


$(TEST_OBJ_DIR)/%.o: %.cpp
$(TEST_OBJ_DIR)/%.o: %.cpp $(TEST_DEP_DIR)/%.d
	mkdir -p $(dir $@)
	mkdir -p $(patsubst build/test/obj/%, build/test/dep/%, $(dir $@))
	$(CXX) $(TEST_DEPFLAGS) $(CXXFLAGS) $(TEST_CXXFLAGS) $(CPPFLAGS) $(EXTERNAL_INCLUDE) -c -o $@ $<


# .PRECIOUS: if make is killed or interrupted during the execution of their recipes, the target is not deleted
.PRECIOUS = $(TEST_DEP_DIR)/%.d
$(TEST_DEP_DIR)/%.d: ;

-include $(TEST_DEPS)
