#!/bin/bash

echo "Initializing submodules"
git submodule init
git submodule update

echo "Build and run tests"
make test
$($LATEST_TEST_BUILD)
build/latest_test

echo "Build debug"
make debug
